# Cookie.JAR

<!--
Short description: objective of the project in one sentence.
-->
Cookie.JAR is a protective barrier and storage location for the hidden snippets one can find on many websites, Cookies.

## Table of Contents

[[_TOC_]]

## Description
<!--
A description of the project follows, which should include a substructure. A good description should not get out of hand, but still help third parties understand the project and understand your design decisions. Describe the importance of the project and what it does. Please add said design decisions (also regarding the hardware used) to this description. The software architecture should be clearly described and all libraries used, ML models used, data used for training, etc.) should be named.
-->

Navigate the internet without 3rd party tracking

Cookies are hidden snippets of code you can find on many websites. You can distinguish between useful 1st party cookies and unwanted 3rd party cookies. These 3rd party trackers collect personal information based on your browsing behaviour. Sexuality, emotional status, finances, medical conditions, credit score – we are often unaware of the extent of this tracking. Over time, data collection companies create a digital avatar that makes you identifiable anywhere on the internet.

Cookie.JAR is both a protective barrier and a storage location: it prevents third party tracking and stores useful first party cookies. Mobile and thus always by your side, Cookie.JAR is connected to all your end devices and intercepts any kind of trackers before they land on your device. As soon as you switch one of your connected devices on and it connects to the internet, Cookie.JAR works in the background. Cookie.JAR recognises and categorises the different forms of cookies and reacts accordingly. 3rd party cookies are blocked and deleted before they reach your device. 1st party cookies can be accepted, blocked, modified or allowed.In addition, the Share option allows you to exchange your settings for useful 1st party cookies with family and friends.

Diligently collecting all 3rd party trackers you come across, Cookie.JAR‘s LED display fills up throughout the day. When it reaches its storage limit, you throw it into the Cookie.run. This empties the storage. The mobile Cookie.JAR recharges and is ready to go again.


## Installation
<!--
Describe how other people can install the project locally. Optionally, you can include a gif to make the process even clearer for other people.
-->
### Requirements
- Arduino IDE (or other solution to flash and, if necessary, change the code on your Arduino)
- IP connection between Raspberry Pi and Linux PC
- Google Chrome as browser (only browser tested yet)

#### Arduino
- Hall sensor
- 10k Ω resistor
- LED strip
- Breadboard and jumper cables (or other solution to connect the objects)
- USB connector for Arduino to Raspberry Pi

#### Raspberry Pi
- NFS Server (we used the nfs-server package)
- Python3 (tested with Python3.9)
##### Python Packages
- sqlite3 (normally comes with Python3)
- pyserial

#### Linux
- a NFS client (we used for an Ubuntu the nfs-common package)

### Raspberry Pi
- install the required software
  via Debian Package Manager `# apt-get install nfs-server`
- configure the NFS server
- (opt) run `arduino_interaction.py` automatically via a systemd unit (example in `./raspberry_pi/config/etc/cookiejar.service`)

#### configure the NFS:
Configure the NFS server on the Raspberry Pi:
1. add the content of ./raspberry_pi/config/etc/fstab to your /etc/fstab file
2. add the content of ./raspberry_pi/config/etc/exports to your /etc/exports file
  and change the IP addresses if necessary or wished so that the network file system is reachable for every device you want to connect to it
If use a different server than the nfs-server package, please configure your server accordingly.

### Arduino
1. Flash code in ./arduino/CookieJar/CookieJar.ino on Arduino
2. Connect the sensor, LED strip and resistor as described in the following schematic
  ![Cookie.JAR schematic](cookiejar_schematic.jpg)
3. Connect Arduino with Raspberry Pi via serial port

### Linux
The installation on the computer is relatively simple.
1. Install the NFS client (we used the nfs-common Debian package)
2. Mount the file system exported by the Raspberry Pi into the directory of your Google Chrome profile. They are located in the directory `$HOME/.config/google-chrome/` and are normally named as `Guest Profile`, `System Profile`or `Profile <number>` depending on your previous usage and settings of Google Chrome.
   You can also mount the NFS automatically, for example via one of the solutions explained here: [https://wiki.archlinux.org/title/NFS#Client](https://wiki.archlinux.org/title/NFS#Client)

## Usage
<!--
Describes briefly how other people can use the project after installation. This would also be a good place to include or reference screenshots of the project in action.
-->
### Raspberry Pi
If you do not run the `arduino_interaction.py` script automatically via a systemd unit, run:
`python ./raspberry_pi/scripts/arduino_interaction.py`

### Linux
You only have to confirm that the NFS is mounted at the right directory. If it does, you can begin to use the Chrome Browser with the Profile shared with the Raspberry Pi.

## Acknowledgments
<!--
Add references to your team etc in this section and link it to contributing people.
-->
Many thanks to the codingIXD project for giving us the inspiration and opportunity to come up with this project. In addition to Felix Groll of the Weißensee elab for helping us with technical questions.

## License
<!--
Add a section for credits: Finally, add a section for the license of the project. For more information on choosing a license, go to: https://choosealicense.com/
-->

    Cookie.JAR
    Copyright (C) 2022  CookieFactory

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
