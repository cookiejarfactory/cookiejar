"""Module for the interaction with cookies in a google_chrome Cookies database.

Functions:
    - get_cookie_num
    - pattern_time2chrome_utc
    - delete_cookies_with_rules
    - read_rules_from_config
    - find_third_party_sql
    - delete_3rd_party_cookies
    - delete_cookies"""

import sqlite3
import re
from datetime import datetime, timedelta, timezone
import time

def get_cookie_num(cookies_file):
    """Returns the number of cookies currently in the database"""
    num_cookies = 0
    connection = sqlite3.connect(f"file:{cookies_file}?mode=ro", uri=True)
    cur = connection.cursor()
    cur.execute("SELECT COUNT(*) FROM cookies;")
    num_cookies = cur.fetchall()[0][0]
    connection.close()
    return num_cookies

def pattern_time2chrome_utc(pattern):
    """Converts a time pattern of a config file \"delete_rules.conf\" (ISO 8601 format) of this program to a Chrome UTC timestamp.

Chromium uses for its timestamps as base the 1. January 1601 00:00:00 UTC . The datetime module that is used for conversion from ISO format to a timestamp uses the UNIX time. So the timestamps have to be converted."""
    # the base date that chrome uses for its timestamps is based on the 1st January 1601 instead of the UNIX time the  datetime module uses
    chromebase_timestamp = int(round(datetime(1601, 1, 1, tzinfo=timezone.utc).timestamp()))
    target_time = datetime.fromisoformat(pattern)
    target_timestamp = target_time.replace(tzinfo=timezone.utc).timestamp()

    return target_timestamp - chromebase_timestamp

def delete_cookies_with_rules(cookies_file, column, pattern, history_file=None):
    """Deletes all cookies where the given column has the given pattern

This function converts the pattern used in the \"delete_rules.conf\" config file into a SQL WHERE rule and deletes all tuples in the cookies database table that fulfill this rule.

Currently unstable for 3rd party cookies cause of unstable function find_third_party_sql"""
    
    # check for key type
    key_type = {
            "creation_utc": datetime,
            "host_key": str,
            "top_frame_site_key": str,
            "name": str,
            "value": str,
            "encrypted_value": None,
            "path": str,
            "expires_utc": datetime,
            "is_secure": int,
            "is_httponly": int,
            "last_access_utc": datetime,
            "has_expires": int,
            "is_persistent": int,
            "priority": int,
            "samesite": int,
            "source_scheme": int,
            "source_port": int,
            "is_same_party": int,
    }

    if column == "3rd_party" and history_file:
        category_filter = find_third_party_sql(history_file)

    elif key_type[column] == str:
        category_filter = f"{column} LIKE '{pattern}'"

    elif key_type[column] == int:
        category_filter = "{} = {}".format(column, int(pattern))

    elif key_type[column] == datetime:
        if pattern[0:1] == '<=': # interval (-inf, target_chrome_utc]
            target_chrome_utc = pattern_time2chrome_utc(pattern[1:])
            category_filter = f"{column} <= {target_chrome_utc}"

        elif pattern[0:1] == '>=': # interval [target_chrome_utc, inf)
            target_chrome_utc = pattern_time2chrome_utc(pattern[1:])
            category_filter = f"{column} >= {target_chrome_utc}"

        elif pattern[0] == '<': # interval (-inf, target_chrome_utc)
            target_chrome_utc = pattern_time2chrome_utc(pattern[1:])
            category_filter = f"{column} < {target_chrome_utc}"

        elif pattern[0] == '>': # interval (target_chrome_utc, inf)
            target_chrome_utc = pattern_time2chrome_utc(pattern[1:])
            category_filter = f"{column} > {target_chrome_utc}"

        elif pattern[0] == '[' and pattern[-1] == ']': # interval [start_time, end_time]
            regex = re.compile("\d{4}-\d{2}-\d{2}.\d{2}:\d{2}:\d{2}")
            found_times = regex.findall(pattern)
            if len(found_times) != 2:
                raise ValueError(f"in {pattern} has to be exactly 2 timestamps")
            start_time = pattern_time2chrome_utc(found_times[0])
            end_time = pattern_time2chrome_utc(found_times[1])
            category_filter = f"{column} BETWEEN {start_time} AND {end_time}"

        else:
            raise ValueError("not compatible pattern for this column is used")
    else:
        raise ValueError("encrypted_value is not supported at this point")

    # create a connection with Cookies file
    cookie_connection = sqlite3.connect(cookies_file)
    cursor = cookie_connection.cursor()
    # execute deletion
    res = cursor.execute(f"DELETE FROM cookies WHERE {category_filter};")
    # commit change and close connection
    cookie_connection.commit()
    cookie_connection.close()

def read_rules_from_config(file):
    """Returns a dictionary of column pattern mappings with the schema column: list(pattern) read from a given \"delete_rules.conf\" configuration file."""

    mapping = dict()
    with open(file, 'r') as f:
        for line in f:
            # jump if line is comment
            if line[0] == '#':
                continue
            
            # split line into table entries "key" "pattern"
            l = line.strip().split()
            key = l[0]; pattern = l[1]
            if not key in mapping.keys():
                mapping[key] = []
            mapping[key].append(pattern)
    return mapping

def find_third_party_sql(history_file, trys=10):
    """Finds an valid SQL WHERE expression to find all third party cookies.

Currently unstable method since google chrome locks history longtime when new sides were visited.
It tries multiple times to execute the SELECT query on the history database and then """

    def isolate_base_pattern(url):
        """Extracts the top-level and second level domains of an full URL and converts them into a SQL pattern"""
        no_prot = url.split('/')[2]
        try:
            sec, top = no_prot.split('.')[-2:]
        except ValueError as e:
            return "%" + no_prot + "%"
        return "%." + sec + "." + top
    
    # get visited URLs
    urls = None
    while urls == None and trys > 0:
        try:
            hist_con = sqlite3.connect(f"file:{history_file}?mode=ro", uri=True, timeout=1)
            hist_cur = hist_con.cursor()
            hist_cur.execute("SELECT url FROM urls WHERE id IN (SELECT DISTINCT url FROM visits);")
            urls = [url[0] for url in hist_cur.fetchall()]
        except sqlite3.OperationalError as e:
            pass
        finally:
            trys -= 1
            # close connection with browser history
            hist_con.close()

    if trys <= 0:
        print(f"{time.asctime()}: Deletion of 3rd party cookies failed. Browser history is currently blocked")
        return "False"

    # create sql pattern rule for visited websites
    base_patterns = list(set(map(isolate_base_pattern, urls)))

    # construct WHERE expression to find all 3rd party cookies
    if len(base_patterns) > 0:
        sql = f"NOT (host_key LIKE '{base_patterns[0]}'"
        for pattern in base_patterns[1:]:
            sql += f"OR host_key LIKE '{pattern}'"

        return sql + ")"
    return "False"

def delete_3rd_party_cookies(cookies_file, history_file):
    """Deletes 3rd party cookies.

Currently unstable because of usage of unstable function delete_cookies_with_rules for 3rd_party cookies"""

    delete_cookies_with_rules(cookies_file, "3rd_party", None, history_file)


def delete_cookies(cookies_file):
    """Deletes cookies that fulfill at least one pattern in the \"delete_rules.conf\" config file"""

    # read the config file
    rules = read_rules_from_config("/home/cookiejar/raspberry_pi/config/delete_rules.conf")
    for key in rules.keys():
        for pattern in rules[key]:
            delete_cookies_with_rules(cookies_file, key, pattern)
