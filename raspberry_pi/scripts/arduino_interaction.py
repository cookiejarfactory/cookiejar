#!/use/bin/env python
"""Script for interaction with the arduino controller

Classes:
    DeleteCookieHandler
    - Handler of the delete-cookie signaling"""


import serial
import time
import threading
import traceback
import interact_cookies

arduino = serial.Serial(port="/dev/ttyACM0", baudrate=9600)
base_dir = "/export/profiles"
database_file = f"{base_dir}/Cookies"
history_file  = f"{base_dir}/History"
lock = threading.Lock()

def send_str(tty, string):
    """Sends a string to the given serial connection"""
    print(f"{time.asctime()}: Send message: '{string}'")
    tty.write(bytes(string, "utf-8"))

class DeleteCookieHandler(threading.Thread):
    """A class used as Handler for the delete-cookie signaling between arduino and the executing system."""

    def __init__(self):
        threading.Thread.__init__(self)
    
    def run(self):
        while True:
            try:
                # Wait for and read from input on arduino
                res = arduino.read()
                lock.acquire()

                print(f"{time.asctime()}: Delete Cookies")
                # delete 3rd party cookies
                interact_cookies.delete_3rd_party_cookies(database_file, history_file)
                # delete with custom rules
                interact_cookies.delete_cookies(database_file)

                # get new number of cookies
                num_cookies_handler = interact_cookies.get_cookie_num(database_file)
                print(f"{time.asctime()}: Number of Cookies after deletion {num_cookies_handler:10}")

                # send num_cookies arduino
                send_str(arduino, str(num_cookies_handler))
            except Exception as e:
                print(f"{time.asctime()}: Exception in DeleteCookieHandler", end=":\n\t")
                traceback.print_exc()
            finally:
                try:
                    lock.release()
                except RuntimeError as e:
                    print(e)

# start delete cookie handler
delete_cookie_handler = DeleteCookieHandler()
delete_cookie_handler.daemon = True
delete_cookie_handler.start()

try:
    while True:
        try:
            if not lock.locked():
                num_cookies = interact_cookies.get_cookie_num(database_file)
                print(f"{time.asctime()}: Number of Cookies {num_cookies:10}")
                send_str(arduino, str(num_cookies))
                time.sleep(5)
            else:
                time.sleep(1)
        except serial.SerialException as e:
            raise e
        except Exception as e:
            print(f"{time.asctime()}: Exception in regular execution", end=":\n\t")
            traceback.print_exc()
finally:
    arduino.close()
