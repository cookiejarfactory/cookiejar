import sqlite3
import os

sqliteConnection = sqlite3.connect('/export/profiles/Cookies')

try:
    cursor = sqliteConnection.cursor()
    res = cursor.execute("SELECT COUNT(*) FROM cookies;")
    print(res.fetchall()[0][0])
except Exception as e:
    print(e)
    sqliteConnection.close()
    quit()
