

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif
 
#define INPUT 2 //Pin for Hall Sensor with Chip
#define INPUT 4 //Pin for small Hall Sensor

#define PIN       6  // Pin for Strip Connection

#define NUMPIXELS 19 // Number of Pixels in Strip
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800); //define Neopixel Strip
#define DELAYVAL 50  // Time (in milliseconds) to pause between Pixels
#define SCALE 500

//Variables for Color Values and Calculations
int input = 0;
int pixelNumber = 0;
int colRed = 0;
int colGreen = 0;
int colBlue = 0;

void setup() {
  Serial.begin(9600);
  pixels.begin();
  
}

void loop() {

  //Hall Sensor detects Magnetic Field
  if ((digitalRead(2)==LOW)){
    
    //notification of raspberry pi via serial port
    Serial.print(42);

    //displaying Light Show until incoming Notification via Serial Port
    do {
      for(int i=0; i<NUMPIXELS; i++) {
      
        pixels.clear();
        pixels.setPixelColor(i, pixels.Color(255, 255, 255));
        pixels.show();
  
        delay(DELAYVAL);
      }
      pixels.clear();
      pixels.show();
    } 
    while(Serial.available() == 0); 
  }
  if (Serial.available() != 0){

    input = Serial.readString().toInt();
    pixelNumber = (input * NUMPIXELS)/SCALE;
    
    pixels.clear();
    
    //determining the Amount of Pixels scaled by "maximal" Amount of collected Cookies
    pixelNumber = (input * NUMPIXELS) / SCALE;
    
    //determining the Color Values
    //blueScale: (240,242,255)(188,218,232)(95,179,212)(0,134,191)(0,87,158)
    //redScale: (255,230,219)(255,171,143)(255,100,69)(227,20,27)(168,0,6)
    if (input<=SCALE/NUMPIXELS*2){
      colRed = 240;
      colGreen = 242;
      colBlue = 255;
    }
    else if ((input>SCALE/NUMPIXELS*2)&&(input<=SCALE/NUMPIXELS*4)){
      colRed = 188;
      colGreen = 218;
      colBlue = 232;
    }
    else if ((input>SCALE/NUMPIXELS*4)&&(input<=SCALE/NUMPIXELS*6)){
      colRed = 95;
      colGreen = 179;
      colBlue = 212;
    }
    else if ((input>SCALE/NUMPIXELS*6)&&(input<=SCALE/NUMPIXELS*8)){
      colRed = 0;
      colGreen = 134;
      colBlue = 191;
    }
    else {
      colRed = 0;
      colGreen = 87;
      colBlue = 158;
    }

    //Edge Case for many Cookies -> would try using more LEDs than available
    if (pixelNumber>=NUMPIXELS) {
      pixelNumber=NUMPIXELS;
    }
    //at least 1 LED is alway active
    //if (pixelNumber<1) {
    //  pixels.setPixelColor(0, pixels.Color(colRed, colGreen, colBlue));
    //}
    
    //setting the Pixels to Color Value
    for(int i=0; i<pixelNumber; i++){
      pixels.setPixelColor(i, pixels.Color(colRed, colGreen, colBlue));
      delay(1);
    }
    pixels.show();
  }
}
